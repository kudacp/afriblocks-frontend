import "bootstrap/dist/css/bootstrap.min.css"
import { createApp } from 'vue/dist/vue.esm-bundler';
import { createPinia } from 'pinia'
import SplitCarousel from "vue-split-carousel";

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(SplitCarousel);

app.mount('#app')
import "bootstrap/dist/js/bootstrap.js"
